{-# OPTIONS_HADDOCK show-extensions,prune #-}
{-# LANGUAGE DisambiguateRecordFields  #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE FunctionalDependencies    #-}
{-# LANGUAGE KindSignatures            #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE Rank2Types                #-}
{-# LANGUAGE TemplateHaskell           #-}

----------------------------------------------------------------------------
-- |
-- | Module        : SWSF.Types
-- | Description   : datatypes used by rest of program
-- | Copyright     : Dmitry Bogatov, 2017
-- | License       : GPL-3+
-- | Maintainer    : KAction@gnu.org
-- | Stability     : experimental
-- | Portability   : POSIX
-- |
----------------------------------------------------------------------------

module SWSF.Types where
import           Control.Lens.Extended
import           Control.Monad.Reader
import           Control.Monad.State
import           Data.Aeson.TH.Extended
import           Data.Cache                     (Cache)
import           Data.List.NonEmpty
import           Data.List.PointedList.Circular
import           Data.Set                       (Set)
import           Stage0
import           SWSF.Types.Basic
import           SWSF.UI.Sprite

type Circular a = PointedList a

----------------------------------------------------------------------------
-- | Economical and battle properties of unit.
-- |
-- | Part of read-only state of game, initialized from
-- | resources/units/*/stats.yaml file.
----------------------------------------------------------------------------
data UnitStats = UnitStats {
  _hitpoints :: Int,
  _speed     :: Int
}

-- | Sprite (forward) and its flopped version (backward)
data Frame = Frame {
  _forward  :: Sprite,
  _backward :: Sprite
  }

----------------------------------------------------------------------------
-- | Representation of unit -- animation and, possibly, sounds.
----------------------------------------------------------------------------
data UnitLook = UnitLook {
  _still :: Frame,
  _move  :: NonEmpty Frame,
  _melee :: NonEmpty Frame
  }

-----------------------------------------
-- Datatype representing unit as whole --
-----------------------------------------
data Unit = Unit {
  _stats :: UnitStats,
  _look  :: UnitLook
  }

----------------------------------------------------------------------------
-- | Hex is basic unit of placement during battle. For interface
-- | reasons, different hexes are rendered different at different
-- | moments of time, but they all must have same size, or graphics
-- | is ruined.
-- |
-- | So far, it is not enforced at type level.
----------------------------------------------------------------------------
newtype Hex = Hex { _sprite :: Sprite }

----------------------------------------------------------------------------
-- | Background is image (sprite), above which all other actions take place.
-- | It does not affect battle, just eye-candy.
----------------------------------------------------------------------------
newtype Background = Background { _sprite :: Sprite }

-------------------------------------------------
-- | Read-only parameters of battle mode grid. --
-------------------------------------------------
data GridInfo = GridInfo
  { _size   :: Coords -- ^ constant: 11 rows, 15 columns
  , _offset :: Point  -- ^ position of top-left corner of the grid.
  }

data RenderFn = RenderFn {
  call :: forall m. MonadIO m => ((Sprite -> Point -> m ()) -> m ()) -> m ()
}

data MovingUnit = MovingUnit {
  _current :: Point,
  _speed   :: Int,
  _dest    :: Coords,
  _frames  :: Circular Frame
}

----------------------------------------------------------------
-- Particular instance of 'Unit's, positioned on battle grid. --
----------------------------------------------------------------
data BattleUnit = BattleUnit {
  _base   :: Unit,
  _state  :: Either MovingUnit Coords,
  _side   :: Side,
  _number :: Int
  }

-- Aggressor is always on left, defender on right.
data Side = Aggressor | Defender

declareRepo ''Background (listPNG "background")
declareRepo ''Hex (listPNG "hex")
declareRepo ''Unit (pure ["archangel", "dwarf", "skeleton"])

----------------------------------------------------------------------------
-- | Read-only configuration of the game. Attempt to separate off
-- | SDL-related stuff proved unsuccessful, so now there is read-only
-- | ('WConf') game data (loaded sprites, for example) and read-write
-- | data ('WState').
----------------------------------------------------------------------------
data WConf = WConf {
  _unit       :: UnitRepo,
  _hex        :: HexRepo,
  _background :: BackgroundRepo,
  _grid       :: GridInfo,
  _delay      :: IO (),
  _render     :: RenderFn
  }

--------------------------------------------------------------------
-- Datatype for information that can be inferred from the rest of --
-- 'WState'.
--------------------------------------------------------------------
data WCache = WCache {
  _accessible :: Cache (ReaderT WConf (StateT WState IO)) (Set Coords)
  }

data WState = WState {
  _units      :: Circular BattleUnit,
  _hexFocused :: Maybe Coords,
  _cache      :: WCache,
  _userQuit   :: Bool
  }

type W a = ReaderT WConf (StateT WState IO) a

makeUnderscoreFields ''Background
makeUnderscoreFields ''BattleUnit
makeUnderscoreFields ''Frame
makeUnderscoreFields ''GridInfo
makeUnderscoreFields ''Hex
makeUnderscoreFields ''MovingUnit
makeUnderscoreFields ''Unit
makeUnderscoreFields ''UnitLook
makeUnderscoreFields ''UnitStats
makeUnderscoreFields ''WCache
makeUnderscoreFields ''WConf
makeUnderscoreFields ''WState

makeLenses ''BackgroundRepo
makeLenses ''HexRepo
makeLenses ''UnitRepo

mkJSON ''UnitStats
mkJSON ''Side
