{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase       #-}
{-# LANGUAGE Rank2Types       #-}
module SWSF.Main.Render (everything) where
import           Control.Lens
import           Control.Monad
import           Data.Bool
import           Data.Cache
import           Data.List.PointedList.Circular
import qualified Data.Set                       as Set
import qualified Graphics.UI.SDL                as S
import qualified SWSF.Extra.Coords              as Coords
import qualified SWSF.Extra.Hex                 as Hex
import           SWSF.Types
import           SWSF.Types.Basic
import qualified SWSF.UI.Font                   as Font
import           SWSF.UI.Sprite                 (Sprite)
import qualified SWSF.UI.Sprite.Align           as Align

-- Type of function passed to sub-computation by `view render'. Since
-- it is only used by rendereing code, and so far rendering code is
-- only in this module, there is no point in moving this type alias
-- into SWSF.Types.
type Put = Sprite -> Point -> W ()

placeHex1 :: Put -> Coords -> W ()
placeHex1 put c_ = Hex.cornerW c_ >>= \pt -> do
  let put' l = join (put <$> view (hex.l.sprite) <*> pure pt)
  put' other

  -- place once again 'other' hex on accessible hexes.
  -- and 'focused' on accesible, focused hex.
  accessible_ <- access $ cache.accessible
  when (Set.member c_ accessible_) $ do
    put' other
    flip when (put' focused) =<< fmap (== Just c_) (use hexFocused)

  let grey = S.Color 150 150 150
  label <- Font.render (c_^._Coords.to show) $ do
    Font.size  .= 12
    Font.color .= grey
  Align.anchor Align.CENTER put label =<< Hex.centerW c_

placeHexGrid :: Put -> W ()
placeHexGrid put = views (grid.size) Coords.rect >>= mapM_ (placeHex1 put)

getVersion :: Side -> Lens' Frame Sprite
getVersion = \case
  Aggressor -> forward
  Defender  -> backward

placeUnit :: Put -> Bool -> BattleUnit -> W ()
placeUnit put active u =
  let put' = Align.anchor Align.UNIT put in
  case view state u of
    Left mp -> do
      destp_ <- Hex.centerW (mp^.dest)
      let direction_ = bool backward forward (mp^.current.x <= destp_^.x)
          sprite_ = view (frames.focus.direction_) mp
      put' sprite_ (view current mp)
    Right c -> do
      p_ <- Hex.centerW c
      let version = views side getVersion u
      put' (u^.base.look.still.version) p_

      let blue = S.Color 0 10 200
          red  = S.Color 200 10 10
          grey = S.Color 150 150 150
          bg   = bool blue red active
      let number_ = view number u
          -- Label looks better when it is larger.
          -- XXX: make label of constant width regardless of number_
          -- XXX: invent a way to relocate label to not clip with other units
          string_ = "  " ++ show number_ ++ "   "

      label_  <- Font.render string_ $ do
        Font.size  .= 16
        Font.color .= grey
        Font.style .= Font.Shaded bg
      Align.anchor (0.5, 0) put label_ (p_ & y +~ 18)

performRendering :: Put -> W ()
performRendering put = do
  flip put (Point 0 0) =<< view (background.grass.sprite)
  placeHexGrid put
  PointedList ls_ x_ rs_ <- use units
  let placeUnit' = placeUnit put
  mapM_ (placeUnit' False) ls_
  mapM_ (placeUnit' False) rs_
  placeUnit' True x_

everything :: W ()
everything = do
  render' <- view render
  call render' performRendering
