{-# LANGUAGE LambdaCase #-}
module SWSF.Main.Event (process, forEach) where
import           Control.Lens.Extended  hiding ((<|))
import           Control.Monad
import           Control.Monad.IO.Class
import           Data.Cache
import           Data.List.NonEmpty     (NonEmpty (..), (<|))
import           Data.List.PointedList
import           Data.Semigroup
import qualified Data.Set               as Set
import qualified Graphics.UI.SDL        as S
import qualified SWSF.Extra.Hex         as Hex
import           SWSF.Types

forEach :: (MonadIO m) => (S.Event -> m ()) -> m ()
forEach f = go where
  go = liftIO S.pollEvent >>= \case
    S.NoEvent -> pure ()
    ev -> f ev >> go

multiply :: Int -> NonEmpty a -> Circular a
multiply n input =
  let clone k a = if k <= 1
                  then a :| []
                  else a <| clone (k-1) a
      x :| xs = sconcat $ fmap (clone n) input
  in PointedList [] x xs

process :: S.Event -> W ()
process = \case
  S.KeyDown S.Keysym {S.symKey = key} ->
    case key of
      S.SDLK_F10 -> userQuit .= True
      _          -> pure ()
  S.MouseMotion x' y' _ _ -> Hex.locateW x' y' >>= (hexFocused .=)
  S.MouseButtonUp x' y' S.ButtonRight ->
    with (units.focus.state._Right) $ \c -> do
      Hex.locateW x' y' >>= \case
        Nothing -> pure ()
        Just dest_ -> do
          accessible_ <- access $ cache.accessible
          when (Set.member dest_ accessible_) $ do
            current_ <- Hex.centerW c
            zoom (units.focus) $ do
              frames_  <- uses (base.look.move) (multiply 5)
              let mp = MovingUnit current_ 16 dest_ frames_
              state .= Left mp
            invalidate $ cache.accessible
  _ -> pure ()
