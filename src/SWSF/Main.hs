{-# LANGUAGE LambdaCase                #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE StandaloneDeriving        #-}
module SWSF.Main where
import           Control.Lens           (use, view)
import           Control.Monad.IO.Class
import           Control.Monad.Loops
import qualified SWSF.Init              as Init
import qualified SWSF.Main.Animation    as Animation
import qualified SWSF.Main.Event        as Event
import qualified SWSF.Main.Render       as Render
import           SWSF.Types

main :: IO ()
main = Init.runW game where
  game = whileM_ (not <$> use userQuit) $ do
    Event.forEach Event.process
    view delay >>= liftIO
    Render.everything
    Animation.invoke
