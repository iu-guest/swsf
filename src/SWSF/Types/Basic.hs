{-# LANGUAGE CPP             #-}
{-# LANGUAGE TemplateHaskell #-}
module SWSF.Types.Basic where
import           Control.Lens
import           Data.Aeson.TH.Extended
import           Data.Group

int :: (Num b, Integral a) => a -> b
int = fromIntegral

------------------------------------------------
-- | Position on SDL Surface.
------------------------------------------------
data Point = Point { _x, _y :: Int } deriving Eq

---------------------------------------------------------------------
-- The only reasonable group instance for Point, useful when doing --
-- arithmetics on coordinates.                                     --
--                                                                 --
-- XXX: Is it time to use 'linear' package?                        --
---------------------------------------------------------------------
instance Semigroup Point where
  Point x1 y1 <> Point x2 y2 = Point (x1 + x2) (y1 + y2)
instance Monoid Point where
  mappend = (<>)
  mempty = Point 0 0
instance Group Point where
  invert (Point x' y') = Point (-x') (-y')
instance Abelian Point

--------------------------------------------------------------
-- | Size of SDL Surface
--------------------------------------------------------------
data Size = Size { _width :: Int, _height :: Int } deriving Eq

----------------------------------------------------------------------------
-- | Position in some kind of matrix or grid.
----------------------------------------------------------------------------
data Coords = Coords { _row, _col :: Int } deriving (Eq, Ord)

makeLenses ''Point
makeLenses ''Size
makeLenses ''Coords
makePrisms ''Coords
mkJSON ''Coords

----------------------------------------------------------------------------
-- | More polymorphic isomorphism for 'Point' than would be generated
-- | by 'makePrisms'. It simplifies interacting with SDL events, where
-- | coordinates are represented by fixed-width words.
----------------------------------------------------------------------------
_Point :: (Integral a, Integral b) => Iso' Point (a, b)
_Point = iso extract construct where
  extract p = (int $ p^.x, int $ p^.y)
  construct (x', y') = Point (int x') (int y')


_Size :: (Integral a, Integral b) => Iso' Size (a, b)
_Size = iso extract construct where
  extract s = (int $ s^.width, int $ s^.height)
  construct (w, h) = Size (int w) (int h)
