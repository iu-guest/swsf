{-|
Module        : SWSF.Extra.Point
Description   : utilities for working with 'Point's
Copyright     : Dmitry Bogatov, 2017
License       : GPL-3+
Maintainer    : KAction@gnu.org
Stability     : experimental
Portability   : POSIX

-}
module SWSF.Extra.Point (distance, walk) where
import           Control.Lens
import           Control.Monad
import           SWSF.Types.Basic


-- | Calculate Manhattan distance between points.
distance :: Point -> Point -> Int
distance p1 p2 = abs(dx p1 p2) + abs(dy p1 p2)

dx :: Point -> Point -> Int
dx p1 p2 = (p2^.x) - (p1^.x)

dy :: Point -> Point -> Int
dy p1 p2 = (p2^.y) - (p1^.y)

-- | Find next point on way to destination. We use Manhattan distance
-- between points to keep away from floating points. Unfortunatelly,
-- there is no single shortest way with such definition of distance,
-- but best efford is made to keep illusion, that Euclidean distance
-- is used instead.
walk :: Point -- ^ destination point
     -> Int   -- ^ how many steps we can walk
     -> Point -- ^ current point
     -> Maybe Point -- ^ next point, closer to destination, or
                    -- Nothing, if destination is reached
walk dest_ n cur_ = do
  let full = distance cur_ dest_
  guard $ full > 0
  pure $ case full <= n of
    True  -> dest_
    False ->
      let [fdx, fdy]   = [dx, dy] <*> [cur_] <*> [dest_]
          [fdx', fdy'] = map abs [fdx, fdy]
          movex' = (n * fdx') `div` (fdx' + fdy')
          movey' = n - movex'
          movex = movex' * signum fdx
          movey = movey' * signum fdy in
        cur_ & x +~ movex & y +~ movey
