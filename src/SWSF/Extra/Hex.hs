{-# LANGUAGE LambdaCase #-}
{-|
Module        : SWSF.Extra.Hex
Description   : utilities for working with hexagonal grid
Copyright     : Dmitry Bogatov, 2017
License       : GPL-3+
Maintainer    : KAction@gnu.org
Stability     : experimental
Portability   : portable

This module is about working with hexagonal grid, covering first
quarter of rectangular coordinate system (@x, y ≥ 0@). Every element
of grid (hex) has a side, parallel Oy axis. While, strictly speaking,
there is mathematically exact ratio between width and height of
containing rectangle unctions in this module accept 'Size' argument
with integer parameter, assuming, but not checking that @width:height@
integer ratio is close enough to that exact value (@√3:2@).

-}

module SWSF.Extra.Hex (
  corner, cornerW,
  center, centerW,
  locate, locateW,
  accessible, accessibleW) where
import           Control.Lens
import           Control.Monad
import           Control.Monad.ST
import           Data.Bool
import           Data.Foldable
import           Data.Group.Extended
import qualified Data.List.NonEmpty             as NE
import           Data.List.PointedList.Circular
import           Data.Semigroup
import           Data.Set                       (Set)
import qualified Data.Set                       as Set
import           Data.STRef
import           Orphans                        ()
import qualified SWSF.Extra.Coords              as Coords
import qualified SWSF.Extra.Point               as Point
import           SWSF.Types                     hiding (accessible)
import           SWSF.Types.Basic
import qualified SWSF.UI.Sprite                 as Sprite

-- | Calculate coordinates of top-left corner of containing rectangle
corner :: Size       -- ^ size of containing rectangle
       -> Coords -- ^ coordinates (indexes in grid) of rectangle
       -> Point
corner size_ coord =
  let width'  = size_^.width
      height' = size_^.height

      i = coord^.row
      j = coord^.col

      x' = j * width' + (if i `mod` 2 == 0 then 0 else width' `div` 2)
      y' = 3 * i * height' `div` 4 in
    Point x' y'

------------------------------------------------------------------------
-- Pure functions, woking with hex grid return coordinates under      --
-- assumption that top-left corner of grid is located at (0, 0).      --
-- Actually, it is at position, specified by GridInfo.corner field of --
-- configuration.                                                     --
--                                                                    --
-- This helper function is used by monadic (with suffix W) functions  --
-- to adjust returned coordinates.                                    --
------------------------------------------------------------------------
shiftGrid :: W Point -> W Point
shiftGrid p = pure (<>) <*> p <*> (view $ grid.offset)

cornerW :: Coords -> W Point
cornerW c = shiftGrid (corner <$> wHexSize <*> pure c)

-- | Calculate coordinates of center of containing rectangle
center :: Size -> Coords -> Point
center size_ coord =
  let corner' = corner size_ coord
      x' = corner'^.x + (size_^.width `div` 2)
      y' = corner'^.y + (size_^.height `div` 2) in
    Point x' y'

wHexSize :: W Size
wHexSize = view $ hex.focused.sprite.to Sprite.size

centerW :: Coords -> W Point
centerW c = shiftGrid (flip center c <$> wHexSize)

locate :: Size -> Point -> Maybe Coords
locate size_ point = do
  let width'  = size_^.width
      height' = size_^.height

      x' = point^.x
      y' = point^.y

      col' = x' `div` width'
      row' = 4 * y' `div` (3 * height')

      distance c = Point.distance point (center size_ c)

      adjacent_ = [Coords i j | j <- [col' - 1.. col' + 1], i <- [row' - 1.. row' + 1]]
      opts = NE.fromList [Min (Arg (distance c) c) | c <- adjacent_]
      Arg _ coords' = getMin $ sconcat opts

  guard $ coords'^.row >= 0
  guard $ coords'^.col >= 0

  pure coords'

locateW :: (Integral a) => a -> a -> W (Maybe Coords)
locateW x_ y_ = do
  offset_    <- view $ grid.offset
  let point_ = _Point # (x_, y_)
  flip locate (point_ <\> offset_) <$> wHexSize >>= \case
    Nothing -> pure Nothing
    Just coords -> bool Nothing (Just coords) <$> Coords.boundedW coords

-- XXX: Brute-forced by conditionals. Invent elegant solution
adjacent :: Coords -> [Coords]
adjacent p_@(Coords row_ _) =
  map (shift p_) (if (even row_) then offsets_even else offsets_odd) where
  shift (Coords u1 v1) (u2, v2) = Coords (u1 + u2) (v1 + v2)
  -- Note: (row, col). It means that "vertical" coordinate is first!
  offsets_odd  = [ (0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (-1, 1) ]
  offsets_even = [ (0, 1), (0, -1), (1, 0), (-1, 0), (1, -1), (-1, -1) ]

-------------------------------------------------------------------------
-- Return set of all positions in hexgrid, accessible by a unit at     --
-- given position with given speed, forbidden to visit specified       --
-- hexes. In essence, it is just wave-front algorithm.                 --
--                                                                     --
-- Complexity: n^2*log n, where n is unit speed.                       --
--                                                                     --
--                                                                     --
-- Complexity can be improve to n^2, using more efficient, but more    --
-- complex algorithm (using matrix instead of set), but for specific   --
-- application, always n <= 16, so it is "only" difference of factor   --
-- four. I doubt this function will ever become a bottleneck, but here --
-- is note for future.                                                 --
-------------------------------------------------------------------------
accessible :: Coords -> Int -> (Coords -> Bool) -> Set Coords
accessible start_ range_ badp_ = runST $ do
  coords_ <- newSTRef $ Set.singleton start_
  let insertAdjacent c_ = forM_ (adjacent c_) $ \adj_ -> do
        unless (badp_ adj_) $ do
          modifySTRef' coords_ $ Set.insert adj_

  forM_ [1..range_] $ const $ do
    readSTRef coords_ >>= traverse_ insertAdjacent

  -- XXX: Ugly corner case. Unit is not allowed to move to same hex,
  -- where it is now, so starting hex must be excluded.
  modifySTRef' coords_ (Set.delete start_)
  readSTRef coords_

-------------------------------------------------------
-- Return set of hexes, accessible for focused unit. --
-------------------------------------------------------
accessibleW :: W (Set Coords)
accessibleW = use (units.focus.state) >>= \case
  Left _ -> {- no "accessible" hexes for moving unit -}
    pure Set.empty
  Right start_ -> zoom units $ do
   range_    <- use $ focus.base.stats.speed
   occupied_ <- use $ traversed.state._Right.to Set.singleton
   pure $ accessible start_ range_ (flip Set.member occupied_)
