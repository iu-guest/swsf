{-|
Module        : SWSF.Extra.Coords
Description   : utilities for Coords datatype
Copyright     : Dmitry Bogatov, 2017
License       : GPL-3+
Maintainer    : KAction@gnu.org
Stability     : experimental
Portability   : portable

-}

module SWSF.Extra.Coords (
  rect,
  bounded,
  boundedW
) where
import           Control.Lens.Extended
import           SWSF.Types
import           SWSF.Types.Basic

-- | Enumerate coordinates in rectangle (0, 0) -- (maxrow, maxcol), inclusive.
rect :: Coords -> [Coords]
rect c = let maxrow = c^.row
             maxcol = c^.col in
           Coords <$> [0..maxrow] <*> [0..maxcol]

-- | Return true iff given coordinates are non-negative and are within
-- bounding box (inclusive).
bounded :: Coords -- ^ coordinates to check
        -> Coords -- ^ bounding box
        -> Bool
bounded c box =
  let row_ = view row c
      col_ = view col c
      boxrow_ = view row box
      boxcol_ = view col box in
    row_ >= 0 && col_ >= 0 && row_ <= boxrow_ && col_ <= boxcol_

-- | Return true iff given coordinates are withing hex grid.
boundedW :: Coords -> W Bool
boundedW c = do
  box_ <- view $ grid.size
  pure $ bounded c box_
