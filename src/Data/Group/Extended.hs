{-|
Module        : Data.Group.Extended
Description   : addition group-related utilites
Copyright     : Dmitry Bogatov, 2017
License       : GPL-3+
Maintainer    : KAction@gnu.org
Stability     : experimental
Portability   : POSIX

-}
module Data.Group.Extended (module Data.Group, (<\>)) where
import           Data.Group

-------------------------------------------------------
-- | Monoidal subtraction: 'a <\> b = a <> invert b' --
-------------------------------------------------------
(<\>) :: (Group m) => m -> m -> m
a <\> b = a `mappend` invert b
