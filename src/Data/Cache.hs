{-|
Module        : Data.Cache
Description   : self-refreshing cache in MonadState
Copyright     : Dmitry Bogatov, 2017
License       : GPL-3+
Maintainer    : KAction@gnu.org
Stability     : experimental
Portability   : GHC

Many applications work in, essentially, one huge MonadState. Ideally,
there should be zero redundancy in it, with all possible values of
that state representing valid program situation. Unfortunatelly, it is
not so easy to achive.

Let us consider example, where whole state of program is just a point, like this.

@
data MyState = MyState { x, y :: Double }
program :: StateT MyState IO ()
@

Now, let us imagine, that program often need to calculate distance
from this point to origin, and, for sake of argument, pretend, that
such calculation is expensive. Then we want to cache resulting value
somehow. Like this.

@
data MyState = MyState { x, y, dist :: Double }
@

But now, there is inter-dependency between fields, and we have to be
very careful to not break it, when changing 'x' or 'y' field. Further
more, it is not wise to recalculate 'dist' on /every/ change of 'x' or
'y', since they can be changed several times in raw, without accessing
'dist'.

Probably, the best option is following representation:

@
data MyState = MyState { x, y :: Double, dist :: Maybe Double }
@

that will work in following way: any code, that changes 'x' or 'y',
set 'dist' to Nothing; any code that access 'dist', recalculates and
set 'dist' to Just value.

This module provides abstraction over described approach.

-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
module Data.Cache (Cache, new, access, invalidate) where
import           Control.Lens
import Control.Monad.State.Class

-------------------------------------------------------------------------
-- | Abstract data type, holding cached result of monadic calculation. --
-------------------------------------------------------------------------
data Cache m a = Cache
  {
    _value   :: Maybe a,
    _refresh :: m a
  }
makeLenses ''Cache

------------------------------------------------------------------------
-- | Create new invalidated cache from some monadic calculation. That --
-- | calculation will be re-run when value is 'access'ed from         --
-- | 'invalidate'd cache.                                             --
-- |                                                                  --
-- | To be useful, that cache must be in the corresponding monad      --
-- | state.                                                           --
------------------------------------------------------------------------
new :: m a -> Cache m a
new = Cache Nothing

---------------------------------------------------------------------------
-- | Invalidate cache, stored in monad state. Next call to 'access' will --
-- | re-run monadic action, specified at cache creation.                 --
---------------------------------------------------------------------------
invalidate :: (MonadState s m)
           => Lens' s (Cache m a) -> m ()
invalidate l = l.value .= Nothing

-------------------------------------------------------
-- | Access value in cache, refreshing it as needed. --
-------------------------------------------------------
access :: (MonadState s m)
       => Lens' s (Cache m a) -> m a
access l = use (l.value) >>= \case
  Just res -> pure res
  Nothing -> do
    res     <- id =<< use (l.refresh)
    l.value .= Just res
    pure res
