/*
 *  Copyright (C) 2017 Dmitry Bogatov <KAction@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>
#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>

/*
 * SDL_gfx library does not provide axes mirroring operation,
 * unfortunately. Surface is rectangular, so it has four symmetry
 * axes: vertical, horizontal and two diagonal.
 *
 * According to ImageMagick terminology, 'flip' is mirroring aganist
 * horizontal axis, 'flop' is mirroring aganist vertical axis. I am
 * not aware of terms for mirroring aganist diagonal axis.
 */

/*
 *  Since every surfaces in use by Haskell code is 32 bit, there is no
 *  point in complicating code and supporting arbitrary surface depth
 */
SDL_Surface*
surface_flop(/* const */ SDL_Surface *source)
{
	assert(source);
	assert(source->format->BitsPerPixel == 32);

	SDL_Surface *new = SDL_DisplayFormatAlpha(source);
	if (new == NULL)
		return NULL;

	SDL_LockSurface(new);

	uint32_t *pixels = new->pixels;
	for (int i = 0; i != new->h; ++i)
		for (int j = 0; j != new->w / 2; ++j) {
			uint32_t *this = pixels + (i * new->w) + j;
			uint32_t *that = pixels + (i * new->w) + (new->w -j);
			uint32_t temp = *this;
			*this = *that;
			*that = temp;
		}


	SDL_UnlockSurface(new);
	return new;
}
