* Every local variable and bindings should have trailing
  underscore. Shadowing do not lead to wrong code, but lens-related
  type errors are not funny.

* 80 columns are sacred.

* Do not argue with default config of ``stylish-haskell``.

* Do not argue with default config of ``haskell-mode``.

* Use camelCaseIdentifiers
