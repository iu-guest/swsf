Here will be a game, free (as in Freedom) clone of Heroes of Might and
Magic III, with scripting language. I want to be as successful, as
Wesnoth.

Implementation ideas:

 * SDL2 as graphical library. It seems to be the most reliable choice.
 * GNU Guile as extension language.
 * Tup build system. Because it rocks.
 * git-annex for storing images.
