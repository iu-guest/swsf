size(25cm);
path p;
for (var i = 0; i != 6; ++i) {
  var phi = pi/6 + pi / 3 * i;
  var pt = (cos(phi), sin(phi));
  p = p -- pt;
}
p = p -- cycle;

fill(p, black + opacity(0.3));
draw(p, linewidth(15) + white + opacity(0.7));
